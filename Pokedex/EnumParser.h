#pragma once

#include <string>
#include <map>
#include <algorithm>

using namespace std;

template <typename T>
class EnumParser
{
	map<string, T> enumMap;
public:
	/**
		Used by Enums to populate the enumMap with string to Enum key value pairs
	*/
	EnumParser() {};

	/**
		Parses str into the specified Enum.

		@return T - Specified Enum
		@throws runtime_error
	*/
	T parseEnum(const string& str)
	{
		map<string, T>::const_iterator iValue = enumMap.find(str);
		if (iValue == enumMap.end())
			throw runtime_error("");
		return iValue->second;
	}

	/**
		Gets a string representation of the Enum.

		@return string - String version of the Enum
		@throws runtime_error
	*/
	string toString(const T& e)
	{
		auto findResult = find_if(begin(enumMap), end(enumMap), [&](const std::pair<string, T> &pair)
		{
			return pair.second == e;
		});
		string foundKey;
		if (findResult == end(enumMap))
			throw runtime_error("");
		return findResult->first;
	}
};
