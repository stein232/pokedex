#pragma once

#include <iostream>
#include <string>

enum class Type {
	Bug,
	Dark,
	Dragon,
	Electric,
	Fairy,
	Fighting,
	Fire,
	Flying,
	Ghost,
	Grass,
	Ground,
	Ice,
	Normal,
	Poison,
	Psychic,
	Rock,
	Steel,
	Water
};

EnumParser<Type>::EnumParser()
{
	enumMap["Bug"] = Type::Bug;
	enumMap["Dark"] = Type::Dark;
	enumMap["Dragon"] = Type::Dragon;
	enumMap["Electric"] = Type::Electric;
	enumMap["Fairy"] = Type::Fairy;
	enumMap["Fighting"] = Type::Fighting;
	enumMap["Fire"] = Type::Fire;
	enumMap["Flying"] = Type::Flying;
	enumMap["Ghost"] = Type::Ghost;
	enumMap["Grass"] = Type::Grass;
	enumMap["Ground"] = Type::Ground;
	enumMap["Ice"] = Type::Ice;
	enumMap["Normal"] = Type::Normal;
	enumMap["Poison"] = Type::Poison;
	enumMap["Psychic"] = Type::Psychic;
	enumMap["Rock"] = Type::Rock;
	enumMap["Steel"] = Type::Steel;
	enumMap["Water"] = Type::Water;
}

inline std::ostream& operator<<(std::ostream& os, const Type& type) {
	EnumParser<Type> typeParser;
	os << typeParser.toString(type);
	return os;
}