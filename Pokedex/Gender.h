#pragma once

#include <iostream>
#include <string>

#include "EnumParser.h"

enum class Gender
{
	Male,
	Female,
	Unknown
};

EnumParser<Gender>::EnumParser()
{
	enumMap["Male"] = Gender::Male;
	enumMap["Female"] = Gender::Female;
	enumMap["Unknown"] = Gender::Unknown;
}

inline std::ostream& operator<<(std::ostream& os, const Gender& gender) {
	EnumParser<Gender> genderParser = EnumParser<Gender>();
	os << genderParser.toString(gender);
	return os;
}