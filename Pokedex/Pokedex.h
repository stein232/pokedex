#pragma once

#include <iostream>
#include <string>
#include <vector>

#include "Pokemon.h"

using namespace std;

class Pokedex
{
public:
	Pokedex();
	~Pokedex();

	/**
		Adds a pokemon to the pokedex.

		@param double - Height of the pokemon
		@param double - Weight of the pokemon
		@param string - Name of the pokemon
		@param vector<Ability> - Abilities of the pokemon
		@param vector<Type> - Types of the pokemon
		@param id = Id of the pokemon, leave it blank to have 1 higher than current highest Id in pokedex
		@return int - Id of the newly created pokemon
	*/
	int addPokemon(double, double, string, vector<Ability>, vector<Type>, int id = -1);

	/**
		Gets all the pokemons in the pokedex.

		@return vector<Pokemon*> - The pokemons in the pokedex
	*/
	vector<Pokemon*> getPokemons() const;
	/**
		Gets a specific pokemon in the pokedex by its Id.

		@return Pokemon* - A pointer to the pokemon with the specified Id, or a 
		nullptr if no pokemon with that Id was found
	*/
	Pokemon* getPokemon(int) const;

private:
	int idCounter_;
	vector<Pokemon*> pokemons_;
};

