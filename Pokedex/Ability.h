#pragma once

#include <iostream>
#include <string>

#include "EnumParser.h"

enum class Ability
{
	Adaptability,
	Aerilate,
	Aftermath,
	Air_Lock,
	Analytic,
	Anger_Point,
	Anticipation,
	Arena_Trap,
	Aroma_Veil,
	Aura_Break,
	Bad_Dreams,
	Battle_Armor,
	Big_Pecks,
	Blaze,
	Bulletproof,
	Cheek_Pouch,
	Chlorophyll,
	Clear_Body,
	Cloud_Nine,
	Color_Change,
	Competitive,
	Compound_Eyes,
	Contrary,
	Cursed_Body,
	Cute_Charm,
	Damp,
	Dark_Aura,
	Defeatist,
	Defiant,
	Delta_Stream,
	Desolate_Land,
	Download,
	Drizzle,
	Drought,
	Dry_Skin,
	Early_Bird,
	Effect_Spore,
	Fairy_Aura,
	Filter,
	Flame_Body,
	Flare_Boost,
	Flash_Fire,
	Flower_Gift,
	Flower_Veil,
	Forecast,
	Forewarn,
	Friend_Guard,
	Frisk,
	Fur_Coat,
	Gale_Wings,
	Gluttony,
	Gooey,
	Grass_Pelt,
	Guts,
	Harvest,
	Healer,
	Heatproof,
	Heavy_Metal,
	Honey_Gather,
	Huge_Power,
	Hustle,
	Hydration,
	Hyper_Cutter,
	Ice_Body,
	Illuminate,
	Illusion,
	Immunity,
	Imposter,
	Infiltrator,
	Inner_Focus,
	Insomnia,
	Intimidate,
	Iron_Barbs,
	Iron_Fist,
	Justified,
	Keen_Eye,
	Klutz,
	Leaf_Guard,
	Levitate,
	Light_Metal,
	Lightning_Rod,
	Limber,
	Liquid_Ooze,
	Magic_Bounce,
	Magic_Guard,
	Magician,
	Magma_Armor,
	Magnet_Pull,
	Marvel_Scale,
	Mega_Launcher,
	Minus,
	Mold_Breaker,
	Moody,
	Motor_Drive,
	Moxie,
	Multiscale,
	Multitype,
	Mummy,
	Natural_Cure,
	No_Guard,
	Normalize,
	Oblivious,
	Overcoat,
	Overgrow,
	Own_Tempo,
	Parental_Bond,
	Pickpocket,
	Pickup,
	Pixilate,
	Plus,
	Poison_Heal,
	Poison_Point,
	Poison_Touch,
	Prankster,
	Pressure,
	Primordial_Sea,
	Protean,
	Pure_Power,
	Quick_Feet,
	Rain_Dish,
	Rattled,
	Reckless,
	Refrigerate,
	Regenerator,
	Rivalry,
	Rock_Head,
	Rough_Skin,
	Run_Away,
	Sand_Force,
	Sand_Rush,
	Sand_Stream,
	Sand_Veil,
	Sap_Sipper,
	Scrappy,
	Serene_Grace,
	Shadow_Tag,
	Shed_Skin,
	Sheer_Force,
	Shell_Armor,
	Shield_Dust,
	Simple,
	Skill_Link,
	Slow_Start,
	Sniper,
	Snow_Cloak,
	Snow_Warning,
	Solar_Power,
	Solid_Rock,
	Soundproof,
	Speed_Boost,
	Stall,
	Stance_Change,
	Static,
	Steadfast,
	Stench,
	Sticky_Hold,
	Storm_Drain,
	Strong_Jaw,
	Sturdy,
	Suction_Cups,
	Super_Luck,
	Swarm,
	Sweet_Veil,
	Swift_Swim,
	Symbiosis,
	Synchronize,
	Tangled_Feet,
	Technician,
	Telepathy,
	Teravolt,
	Thick_Fat,
	Tinted_Lens,
	Torrent,
	Tough_Claws,
	Toxic_Boost,
	Trace,
	Truant,
	Turboblaze,
	Unaware,
	Unburden,
	Unnerve,
	Victory_Star,
	Vital_Spirit,
	Volt_Absorb,
	Water_Absorb,
	Water_Veil,
	Weak_Armor,
	White_Smoke,
	Wonder_Guard,
	Wonder_Skin,
	Zen_Mode
};

EnumParser<Ability>::EnumParser()
{
	enumMap["Adaptability"] = Ability::Adaptability;
	enumMap["Aerilate"] = Ability::Aerilate;
	enumMap["Aftermath"] = Ability::Aftermath;
	enumMap["Air Lock"] = Ability::Air_Lock;
	enumMap["Analytic"] = Ability::Analytic;
	enumMap["Anger Point"] = Ability::Anger_Point;
	enumMap["Anticipation"] = Ability::Anticipation;
	enumMap["Arena Trap"] = Ability::Arena_Trap;
	enumMap["Aroma Veil"] = Ability::Aroma_Veil;
	enumMap["Aura Break"] = Ability::Aura_Break;
	enumMap["Bad Dreams"] = Ability::Bad_Dreams;
	enumMap["Battle Armor"] = Ability::Battle_Armor;
	enumMap["Big Pecks"] = Ability::Big_Pecks;
	enumMap["Blaze"] = Ability::Blaze;
	enumMap["Bulletproof"] = Ability::Bulletproof;
	enumMap["Cheek Pouch"] = Ability::Cheek_Pouch;
	enumMap["Chlorophyll"] = Ability::Chlorophyll;
	enumMap["Clear Body"] = Ability::Clear_Body;
	enumMap["Cloud Nine"] = Ability::Cloud_Nine;
	enumMap["Color Change"] = Ability::Color_Change;
	enumMap["Competitive"] = Ability::Competitive;
	enumMap["Compound Eyes"] = Ability::Compound_Eyes;
	enumMap["Contrary"] = Ability::Contrary;
	enumMap["Cursed Body"] = Ability::Cursed_Body;
	enumMap["Cute Charm"] = Ability::Cute_Charm;
	enumMap["Damp"] = Ability::Damp;
	enumMap["Dark Aura"] = Ability::Dark_Aura;
	enumMap["Defeatist"] = Ability::Defeatist;
	enumMap["Defiant"] = Ability::Defiant;
	enumMap["Delta Stream"] = Ability::Delta_Stream;
	enumMap["Desolate Land"] = Ability::Desolate_Land;
	enumMap["Download"] = Ability::Download;
	enumMap["Drizzle"] = Ability::Drizzle;
	enumMap["Drought"] = Ability::Drought;
	enumMap["Dry Skin"] = Ability::Dry_Skin;
	enumMap["Early Bird"] = Ability::Early_Bird;
	enumMap["Effect Spore"] = Ability::Effect_Spore;
	enumMap["Fairy Aura"] = Ability::Fairy_Aura;
	enumMap["Filter"] = Ability::Filter;
	enumMap["Flame Body"] = Ability::Flame_Body;
	enumMap["Flare Boost"] = Ability::Flare_Boost;
	enumMap["Flash Fire"] = Ability::Flash_Fire;
	enumMap["Flower Gift"] = Ability::Flower_Gift;
	enumMap["Flower Veil"] = Ability::Flower_Veil;
	enumMap["Forecast"] = Ability::Forecast;
	enumMap["Forewarn"] = Ability::Forewarn;
	enumMap["Friend Guard"] = Ability::Friend_Guard;
	enumMap["Frisk"] = Ability::Frisk;
	enumMap["Fur Coat"] = Ability::Fur_Coat;
	enumMap["Gale Wings"] = Ability::Gale_Wings;
	enumMap["Gluttony"] = Ability::Gluttony;
	enumMap["Gooey"] = Ability::Gooey;
	enumMap["Grass Pelt"] = Ability::Grass_Pelt;
	enumMap["Guts"] = Ability::Guts;
	enumMap["Harvest"] = Ability::Harvest;
	enumMap["Healer"] = Ability::Healer;
	enumMap["Heatproof"] = Ability::Heatproof;
	enumMap["Heavy Metal"] = Ability::Heavy_Metal;
	enumMap["Honey Gather"] = Ability::Honey_Gather;
	enumMap["Huge Power"] = Ability::Huge_Power;
	enumMap["Hustle"] = Ability::Hustle;
	enumMap["Hydration"] = Ability::Hydration;
	enumMap["Hyper Cutter"] = Ability::Hyper_Cutter;
	enumMap["Ice Body"] = Ability::Ice_Body;
	enumMap["Illuminate"] = Ability::Illuminate;
	enumMap["Illusion"] = Ability::Illusion;
	enumMap["Immunity"] = Ability::Immunity;
	enumMap["Imposter"] = Ability::Imposter;
	enumMap["Infiltrator"] = Ability::Infiltrator;
	enumMap["Inner Focus"] = Ability::Inner_Focus;
	enumMap["Insomnia"] = Ability::Insomnia;
	enumMap["Intimidate"] = Ability::Intimidate;
	enumMap["Iron Barbs"] = Ability::Iron_Barbs;
	enumMap["Iron Fist"] = Ability::Iron_Fist;
	enumMap["Justified"] = Ability::Justified;
	enumMap["Keen Eye"] = Ability::Keen_Eye;
	enumMap["Klutz"] = Ability::Klutz;
	enumMap["Leaf Guard"] = Ability::Leaf_Guard;
	enumMap["Levitate"] = Ability::Levitate;
	enumMap["Light Metal"] = Ability::Light_Metal;
	enumMap["Lightning Rod"] = Ability::Lightning_Rod;
	enumMap["Limber"] = Ability::Limber;
	enumMap["Liquid Ooze"] = Ability::Liquid_Ooze;
	enumMap["Magic Bounce"] = Ability::Magic_Bounce;
	enumMap["Magic Guard"] = Ability::Magic_Guard;
	enumMap["Magician"] = Ability::Magician;
	enumMap["Magma Armor"] = Ability::Magma_Armor;
	enumMap["Magnet Pull"] = Ability::Magnet_Pull;
	enumMap["Marvel Scale"] = Ability::Marvel_Scale;
	enumMap["Mega Launcher"] = Ability::Mega_Launcher;
	enumMap["Minus"] = Ability::Minus;
	enumMap["Mold Breaker"] = Ability::Mold_Breaker;
	enumMap["Moody"] = Ability::Moody;
	enumMap["Motor Drive"] = Ability::Motor_Drive;
	enumMap["Moxie"] = Ability::Moxie;
	enumMap["Multiscale"] = Ability::Multiscale;
	enumMap["Multitype"] = Ability::Multitype;
	enumMap["Mummy"] = Ability::Mummy;
	enumMap["Natural Cure"] = Ability::Natural_Cure;
	enumMap["No Guard"] = Ability::No_Guard;
	enumMap["Normalize"] = Ability::Normalize;
	enumMap["Oblivious"] = Ability::Oblivious;
	enumMap["Overcoat"] = Ability::Overcoat;
	enumMap["Overgrow"] = Ability::Overgrow;
	enumMap["Own Tempo"] = Ability::Own_Tempo;
	enumMap["Parental Bond"] = Ability::Parental_Bond;
	enumMap["Pickpocket"] = Ability::Pickpocket;
	enumMap["Pickup"] = Ability::Pickup;
	enumMap["Pixilate"] = Ability::Pixilate;
	enumMap["Plus"] = Ability::Plus;
	enumMap["Poison Heal"] = Ability::Poison_Heal;
	enumMap["Poison Point"] = Ability::Poison_Point;
	enumMap["Poison Touch"] = Ability::Poison_Touch;
	enumMap["Prankster"] = Ability::Prankster;
	enumMap["Pressure"] = Ability::Pressure;
	enumMap["Primordial Sea"] = Ability::Primordial_Sea;
	enumMap["Protean"] = Ability::Protean;
	enumMap["Pure Power"] = Ability::Pure_Power;
	enumMap["Quick Feet"] = Ability::Quick_Feet;
	enumMap["Rain Dish"] = Ability::Rain_Dish;
	enumMap["Rattled"] = Ability::Rattled;
	enumMap["Reckless"] = Ability::Reckless;
	enumMap["Refrigerate"] = Ability::Refrigerate;
	enumMap["Regenerator"] = Ability::Regenerator;
	enumMap["Rivalry"] = Ability::Rivalry;
	enumMap["Rock Head"] = Ability::Rock_Head;
	enumMap["Rough Skin"] = Ability::Rough_Skin;
	enumMap["Run Away"] = Ability::Run_Away;
	enumMap["Sand Force"] = Ability::Sand_Force;
	enumMap["Sand Rush"] = Ability::Sand_Rush;
	enumMap["Sand Stream"] = Ability::Sand_Stream;
	enumMap["Sand Veil"] = Ability::Sand_Veil;
	enumMap["Sap Sipper"] = Ability::Sap_Sipper;
	enumMap["Scrappy"] = Ability::Scrappy;
	enumMap["Serene Grace"] = Ability::Serene_Grace;
	enumMap["Shadow Tag"] = Ability::Shadow_Tag;
	enumMap["Shed Skin"] = Ability::Shed_Skin;
	enumMap["Sheer Force"] = Ability::Sheer_Force;
	enumMap["Shell Armor"] = Ability::Shell_Armor;
	enumMap["Shield Dust"] = Ability::Shield_Dust;
	enumMap["Simple"] = Ability::Simple;
	enumMap["Skill Link"] = Ability::Skill_Link;
	enumMap["Slow Start"] = Ability::Slow_Start;
	enumMap["Sniper"] = Ability::Sniper;
	enumMap["Snow Cloak"] = Ability::Snow_Cloak;
	enumMap["Snow Warning"] = Ability::Snow_Warning;
	enumMap["Solar Power"] = Ability::Solar_Power;
	enumMap["Solid Rock"] = Ability::Solid_Rock;
	enumMap["Soundproof"] = Ability::Soundproof;
	enumMap["Speed Boost"] = Ability::Speed_Boost;
	enumMap["Stall"] = Ability::Stall;
	enumMap["Stance Change"] = Ability::Stance_Change;
	enumMap["Static"] = Ability::Static;
	enumMap["Steadfast"] = Ability::Steadfast;
	enumMap["Stench"] = Ability::Stench;
	enumMap["Sticky Hold"] = Ability::Sticky_Hold;
	enumMap["Storm Drain"] = Ability::Storm_Drain;
	enumMap["Strong Jaw"] = Ability::Strong_Jaw;
	enumMap["Sturdy"] = Ability::Sturdy;
	enumMap["Suction Cups"] = Ability::Suction_Cups;
	enumMap["Super Luck"] = Ability::Super_Luck;
	enumMap["Swarm"] = Ability::Swarm;
	enumMap["Sweet Veil"] = Ability::Sweet_Veil;
	enumMap["Swift Swim"] = Ability::Swift_Swim;
	enumMap["Symbiosis"] = Ability::Symbiosis;
	enumMap["Synchronize"] = Ability::Synchronize;
	enumMap["Tangled Feet"] = Ability::Tangled_Feet;
	enumMap["Technician"] = Ability::Technician;
	enumMap["Telepathy"] = Ability::Telepathy;
	enumMap["Teravolt"] = Ability::Teravolt;
	enumMap["Thick Fat"] = Ability::Thick_Fat;
	enumMap["Tinted Lens"] = Ability::Tinted_Lens;
	enumMap["Torrent"] = Ability::Torrent;
	enumMap["Tough Claws"] = Ability::Tough_Claws;
	enumMap["Toxic Boost"] = Ability::Toxic_Boost;
	enumMap["Trace"] = Ability::Trace;
	enumMap["Truant"] = Ability::Truant;
	enumMap["Turboblaze"] = Ability::Turboblaze;
	enumMap["Unaware"] = Ability::Unaware;
	enumMap["Unburden"] = Ability::Unburden;
	enumMap["Unnerve"] = Ability::Unnerve;
	enumMap["Victory Star"] = Ability::Victory_Star;
	enumMap["Vital Spirit"] = Ability::Vital_Spirit;
	enumMap["Volt Absorb"] = Ability::Volt_Absorb;
	enumMap["Water Absorb"] = Ability::Water_Absorb;
	enumMap["Water Veil"] = Ability::Water_Veil;
	enumMap["Weak Armor"] = Ability::Weak_Armor;
	enumMap["White Smoke"] = Ability::White_Smoke;
	enumMap["Wonder Guard"] = Ability::Wonder_Guard;
	enumMap["Wonder Skin"] = Ability::Wonder_Skin;
	enumMap["Zen Mode"] = Ability::Zen_Mode;
}

inline std::ostream& operator<<(std::ostream& os, const Ability& ability) {
	EnumParser<Ability> abilityParser;
	os << abilityParser.toString(ability);
	return os;
}