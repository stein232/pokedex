#include "Pokedex.h"



Pokedex::Pokedex()
{
	idCounter_ = 0;
}


Pokedex::~Pokedex()
{
	for (vector<Pokemon*>::const_iterator i = pokemons_.cbegin(); i != pokemons_.cend(); i++)
	{
		delete *i;
	}
}

int Pokedex::addPokemon(double height, double weight, string name, vector<Ability> abilites, vector<Type> types, int id)
{
	if (id == -1) id = ++idCounter_;
	else if (id > idCounter_) idCounter_ = id;
	else id = ++idCounter_;
	Pokemon* pokemon = new Pokemon(id, height, weight, name);
	pokemon->abilities() = abilites;
	//pokemon->genders() = genders;
	pokemon->types() = types;
	//pokemon->weaknesses() = weaknesses;

	pokemons_.push_back(pokemon);
	return id;
}

vector<Pokemon*> Pokedex::getPokemons() const
{
	return pokemons_;
}

Pokemon * Pokedex::getPokemon(int i) const
{
	for (vector<Pokemon*>::const_iterator it = pokemons_.begin(); it != pokemons_.end(); ++it)
	{
		if ((*it)->number() == i) return *it;
	}

	return nullptr;
}
