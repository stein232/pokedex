#pragma once

#include <string>
#include <vector>

#include "Ability.h"
#include "Gender.h"
#include "Type.h"

using namespace std;

class Pokemon
{
public:
	
	Pokemon(int, double, double, string);

	/**
		Gets the Id of the pokemon.

		@return int - Id of the pokemon
	*/
	int number() const;
	/**
		Sets the Id of the pokemon.

		@param int - New Id of the pokemon
	*/
	void number(int);

	/**
		Gets the height of the pokemon.

		@return double - Height of the pokemon
	*/
	double height() const;
	/**
		Sets the height of the pokemon.

		@param double - New height of the pokemon
	*/
	void height(double);

	/**
		Gets the weight of the pokemon.

		@return double - Weight of the pokemon
	*/
	double weight() const;
	/**
		Sets the weight of the pokemon.

		@param double - New weight of the pokemon
	*/
	void weight(double);

	/**
		Gets the name of the pokemon.

		@return string - Name of the pokemon
	*/
	string name() const;
	/**
		Sets the name of the pokemon.

		@param string - New name of the pokemon
	*/
	void name(string);

	//string category() const;
	//void category(string);

	/**
		Gets a reference to the abilities of the pokemon.

		@return vector<Ability>& - Abilites of the pokemon
	*/
	vector<Ability>& abilities();

	/*vector<Gender>& genders();*/

	/**
		Gets a reference to the types of the pokemon.

		@return vector<Type>& - Types of the pokemon
	*/
	vector<Type>& types();

	//vector<Type>& weaknesses();
	
	//vector<Pokemon>& evolutions();

	inline bool operator<(const Pokemon&);
	inline bool operator>(const Pokemon&);
	inline bool operator<=(const Pokemon&);
	inline bool operator>=(const Pokemon&);
	inline bool operator==(const Pokemon&);
	inline bool operator!=(const Pokemon&);

	// Provides details of the pokemon to the stream.
	friend ostream& operator<<(ostream&, const Pokemon&);

private:
	int number_;
	double height_, weight_;
	string name_, category_;
	vector<Ability> abilities_;
	vector<Gender> genders_;
	vector<Type> types_;
	vector<Type> weaknesses_;
	vector<Pokemon> evolutions_;
};