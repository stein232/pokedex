// Pokedex.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "json.hpp"
#include "EnumParser.h"

#include <vector>
#include <fstream>
#include <sstream>
#include <Windows.h>
#include <conio.h>

using json = nlohmann::json;


// Declare functions up here just cos I want main method to be near top
void loadPokemons(Pokedex&);
void showPokemons(const vector<Pokemon*>, string);
void showPokemonsHelper(const vector<Pokemon*>, int);
void showSearchPokemonDialog();
void searchByName();
void searchByHeight();
void searchByWeight();
void searchByAbility();
void searchByType();
void showAddPokemonDialog();
void showEditPokemonDialog();
void toggleAlphabeticalSort();
void sortAlphabetically(vector<Pokemon*>& pokemons);
string capitalise(string&);
string removeHypen(string&);
bool tryParseInt(string);
bool tryParseDouble(string str);

// Variables to be used globally
bool isSortAlphabetically;
Pokedex pokedex;

/**
	The fucking main method for fuck sake, are you retarded?

	@return a number represting the result of the operation
	- 0 = Successful
	- Anything else = Fucked up?
*/
int main()
{
	loadPokemons(pokedex);

	while (1)
	{
		cout << "===== Pokedex v1.5 =====" << endl
			<< endl
			<< "1. See all pokemons" << endl
			<< "2. Search for pokemon" << endl
			<< "3. Add a pokemon" << endl
			<< "4. Edit a pokemon" << endl
			<< "5. Toggle alphabetical sort (" << (isSortAlphabetically ? "On" : "Off") << ")" << endl
			<< "0. Exit pokedex" << endl
			<< endl;

		int choice;
		string input;
		while (true)
		{
			cout << ">>> ";
			getline(cin, input);
			if (!tryParseInt(input))
			{
				cout << "Invalid input" << endl;
				continue;
			}
			
			choice = stoi(input);
			
			if (choice < 0 || choice > 5) {
				cout << "Invalid input" << endl;
				continue;
			}
			break;
		}

		cout << endl;
		switch (choice)
		{
			case 0:
				cout << "Thanks for using Pokedex faggot" << endl;
				return 0;
			case 1:
				showPokemons(pokedex.getPokemons(), "No pokemons to show");
				break;
			case 2:
				showSearchPokemonDialog();
				break;
			case 3:
				showAddPokemonDialog();
				break;
			case 4:
				showEditPokemonDialog();
				break;
			case 5:
				toggleAlphabeticalSort();
				break;
			default:
				cout << "you fucking faggot" << endl;
		}
	}
	
	//vector<char> strings;
	//strings.push_back('a');
	//strings.push_back('b');
	//strings.push_back('c');
	//strings.push_back('d');
	//strings.push_back('e');

	//string a(strings.begin(), strings.end());

	//cout << a << endl;

	return 0;
}

/**
	Loads pokemons into the pokedex by reading a json file
	called 'pokemon.json' in the data folder.

	@param pokedex - Pokedex to load the pokemons into
*/
void loadPokemons(Pokedex& pokedex)
{
	ifstream pokemonJsonFile("data/pokemon.json");

	// first arguement has brackets so that compiler does not think this is a function declaration
	string pokemonJsonString((istreambuf_iterator<char>(pokemonJsonFile)), istreambuf_iterator<char>());
	json pokemonJson = json::parse(pokemonJsonString.c_str());

	EnumParser<Ability> abilityParser;
	EnumParser<Type> typeParser;

	for (size_t i = 0; i < pokemonJson.size(); i++)
	{
		json p = pokemonJson[i];
		int id = p["id"];
		double height = p["height"];
		double weight = p["weight"];
		string name = p["name"];
		//string category = p["category"];
		vector<string> abilitiesStr = p["abilities"];
		vector<string> typesStr = p["types"];

		vector<Ability> abilities;
		for (vector<string>::iterator it = abilitiesStr.begin(); it != abilitiesStr.end(); ++it)
		{
			abilities.push_back(abilityParser.parseEnum(capitalise(removeHypen(*it))));
		}

		vector<Type> types;
		for (vector<string>::iterator it = typesStr.begin(); it != typesStr.end(); ++it)
		{
			types.push_back(typeParser.parseEnum(capitalise(removeHypen(*it))));
		}

		pokedex.addPokemon(height, weight, capitalise(name), abilities, types, id);
	}
}

/**
	A function to show pokemons in a list and allows for navigating
	via the arrow keys.

	@param pokemons - The vector of pokemons to show
	@param emptyMessage - Message to output if pokemons is empty
*/
void showPokemons(vector<Pokemon*> pokemons, string emptyMessage)
{
	if (isSortAlphabetically)
		sortAlphabetically(pokemons);
	if (pokemons.empty())
	{
		cout << emptyMessage << endl;
		return;
	}

	using ::setw;
	
	int rows = 20;
	int page = 0;

	int maxPage = pokemons.size() / rows - 1;
	if (pokemons.size() % rows != 0) maxPage++;

	int pokemonId = -1;

	showPokemonsHelper(pokemons, page);

	cout << endl
		<< "Left and right arrow keys to navigate the list of pokemons" << endl
		<< "Press ESC to go back to the menu" << endl
		<< "and the 'i' key to enter the id of the pokemon you are interested in" << endl
		<< endl;

	while (1)
	{
		if (GetAsyncKeyState(0x27)) // right arrow key
		{
			if (page < maxPage)
				showPokemonsHelper(pokemons, ++page);
		}
		else if (GetAsyncKeyState(0x25)) // left arrow key
		{
			if (page > 0)
				showPokemonsHelper(pokemons, --page);
		}
		else if (GetAsyncKeyState(0x1B)) // escape key
		{
			break;
		}
		else if (GetAsyncKeyState(0x49)) // i key
		{
			cout << "Enter the pokemon id: " << endl 
				<< ">>> ";
			string input;
			getline(cin,input);

			if (input.compare("back") == 0) break;

			if (!tryParseInt(input))
			{
				cout << "This ain't even a number faggot" << endl;
				continue;
			}
			pokemonId = stoi(input);
			Pokemon* p = pokedex.getPokemon(pokemonId);
			if (p == nullptr)
			{
				cout << "Found no pokemon with this id" << endl;
				continue;
			}

			cout << endl
				<< *p << endl
				<< "Press any key to return to the menu" << endl;
			while (1) {
				if (_kbhit())
					break;
			}
			break;
		}
	}

	cout << right;
}

/**
	A helper function for showPokemons. This does the actual displaying.

	@param pokemons - The vector of pokemons to show
	@param page - The page in the list to display
*/
void showPokemonsHelper(const vector<Pokemon*> pokemons, int page)
{
	int rows = 20;
	int TAB = 4;
	int maxPage = pokemons.size() / rows - 1;
	if (pokemons.size() % rows != 0) maxPage++;

	vector<Pokemon*>::const_iterator startIter = pokemons.begin();
	advance(startIter, page * rows);
	vector<Pokemon*>::const_iterator endIter = startIter;
	if (page == maxPage)
		endIter = pokemons.end();
	else
		advance(endIter, rows);

	cout << std::right << setw(20) << "Page: " << page + 1 << "/" << maxPage + 1 << endl;
	cout << endl;
	std::cout << left          //left justified
		<< setw(TAB) << "Id"
		<< setw(TAB) << "Name" << endl;

	for (startIter; startIter != endIter && startIter != pokemons.end(); ++startIter)
	{
		cout << left
			<< setw(TAB) << (*startIter)->number()
			<< setw(TAB) << (*startIter)->name() << endl;
	}

	cout << endl;
}

/**
	Shows a dialog asking the user how they would like to search.
*/
void showSearchPokemonDialog()
{
	while (1)
	{
		cout << "How do you want to search?" << endl
			<< "1. By name" << endl
			<< "2. By Height" << endl
			<< "3. By Weight" << endl
			<< "4. By Ability" << endl
			<< "5. By Type" << endl
			<< "0. Exit search" << endl;

		int choice;
		string input;
		while (true)
		{
			cout << ">>> ";
			getline(cin, input);
			if (!tryParseInt(input))
			{
				cout << "Invalid input" << endl;
				continue;
			}

			choice = stoi(input);

			if (choice < 0 || choice > 5) {
				cout << "Invalid input" << endl;
				continue;
			}
			break;
		}

		cout << endl;
		switch (choice)
		{
			case 0:
				return;
				break;
			case 1:
				searchByName();
				break;
			case 2:
				searchByHeight();
				break;
			case 3:
				searchByWeight();
				break;
			case 4:
				searchByAbility();
				break;
			case 5:
				searchByType();
				break;
			default:
				cout << "you fucking faggot" << endl;
		}
	}
}

/**
	A helper function of showSearchPokemonDialog. Shows a dialog
	asking the user for a string to be searched.
*/
void searchByName()
{
	string name;
	cout << "Enter a string:" << endl
		<< ">>> ";
	getline(cin, name);

	vector<Pokemon*> pokemons;
	vector<Pokemon*> allPokemons = pokedex.getPokemons();
	for (vector<Pokemon*>::iterator it = allPokemons.begin(); it != allPokemons.end(); ++it)
	{
		if ((*it)->name().find(name) != string::npos)
			pokemons.push_back(*it);
	}

	showPokemons(pokemons, "No pokemons with " + name + " found");
}

/**
	A helper function of showSearchPokemonDialog. Shows a dialog
	asking the user for a range of height to search within.
*/
void searchByHeight()
{
	string input;
	double height1;
	while (1)
	{
		cout << "Starting height:" << endl
			<< ">>> ";
		getline(cin, input);
		if (!tryParseInt(input))
		{
			cout << "Invalid height" << endl;
			continue;
		}

		height1 = stod(input);
		if (height1 <= 0)
		{
			cout << "Don't give me negative faggot" << endl;
			continue;
		}
		break;
	}

	cout << endl;

	double height2;
	while (1)
	{
		cout << "Ending height:" << endl
			<< ">>> ";
		getline(cin, input);
		if (!tryParseInt(input))
		{
			cout << "Invalid height" << endl;
			continue;
		}

		height2 = stod(input);
		if (height2 < height1)
		{
			cout << "Ending height should be bigger than starting height faggot" << endl;
			continue;
		}
		break;
	}

	vector<Pokemon*> pokemons;
	vector<Pokemon*> allPokemons = pokedex.getPokemons();
	for (vector<Pokemon*>::iterator it = allPokemons.begin(); it != allPokemons.end(); ++it)
	{
		if ((*it)->height() >= height1 && (*it)->height() <= height2)
			pokemons.push_back(*it);
	}

	showPokemons(pokemons, "No pokemons with height between " + to_string(height1) + "m to " + to_string(height2) + "m found");
}

/**
	A helper function of showSearchPokemonDialog. Shows a dialog
	asking the user for a range of weight to search within.
*/
void searchByWeight()
{
	string input;
	double weight1;
	while (1)
	{
		cout << "Starting weight:" << endl
			<< ">>> ";
		getline(cin, input);
		if (!tryParseInt(input))
		{
			cout << "Invalid weight" << endl;
			continue;
		}

		weight1 = stod(input);
		if (weight1 <= 0)
		{
			cout << "Don't give me negative faggot" << endl;
			continue;
		}
		break;
	}

	cout << endl;

	double weight2;
	while (1)
	{
		cout << "Ending weight:" << endl
			<< ">>> ";
		getline(cin, input);
		if (!tryParseInt(input))
		{
			cout << "Invalid weight" << endl;
			continue;
		}

		weight2 = stod(input);
		if (weight2 < weight1)
		{
			cout << "Ending weight should be bigger than starting weight faggot" << endl;
			continue;
		}
		break;
	}

	vector<Pokemon*> pokemons;
	vector<Pokemon*> allPokemons = pokedex.getPokemons();
	for (vector<Pokemon*>::iterator it = allPokemons.begin(); it != allPokemons.end(); ++it)
	{
		if ((*it)->weight() >= weight1 && (*it)->weight() <= weight2)
			pokemons.push_back(*it);
	}

	showPokemons(pokemons, "No pokemons with weight between " + to_string(weight1) + "kg to " + to_string(weight2) + "kg found");
}

/**
	A helper function of showSearchPokemonDialog. Shows a dialog
	asking the user for a collection of Abilities to be searched.
*/
void searchByAbility()
{
	EnumParser<Ability> abilityParser;
	string ability;
	vector<Ability> abilitiesToHave;
	int counter = 1;
	while (1)
	{
		cout << "Ability " << counter << ":" << endl
			<< ">>> ";
		getline(cin, ability);
		if (ability.compare("-1") == 0) break;
		
		cout << endl;
		
		try
		{
			abilitiesToHave.push_back(abilityParser.parseEnum(capitalise(ability)));
		}
		catch (runtime_error e)
		{
			cout << "Fucking give me a legit ability faggot" << endl
				<< endl;
			continue;
		}
		counter++;
		cout << "Enter another ability or -1 to stop" << endl;
	}

	vector<Pokemon*> pokemons;
	vector<Pokemon*> allPokemons = pokedex.getPokemons();
	for (vector<Pokemon*>::iterator pit = allPokemons.begin(); pit != allPokemons.end(); ++pit)
	{
		bool haveAll = true;
		for (vector<Ability>::iterator ait = abilitiesToHave.begin(); ait != abilitiesToHave.end(); ++ait)
		{
			if (find((*pit)->abilities().begin(), (*pit)->abilities().end(), *ait) == (*pit)->abilities().end())
			{
				haveAll = false;
				break;
			}
		}
		if (haveAll)
			pokemons.push_back(*pit);
	}
	
	if (abilitiesToHave.empty())
	{
		cout << "You fucking faggot, you should have entered at least 1 Ability" << endl;
		return;
	}

	stringstream ss;
	ss << "Found no pokemons with abilities: ";
	for (vector<Ability>::iterator ait = abilitiesToHave.begin(); ait != abilitiesToHave.end() - 1; ++ait)
	{
		ss << *ait << ", ";
	}
	ss << *(abilitiesToHave.end() - 1);
	showPokemons(pokemons, ss.str());
}

/**
	A helper function of showSearchPokemonDialog. Shows a dialog
	asking the user for a collection of Types to be searched.
*/
void searchByType()
{
	EnumParser<Type> typeParser;
	string type;
	vector<Type> typesToHave;
	int counter = 1;
	while (1)
	{
		cout << "Type " << counter << ":" << endl
			<< ">>> ";
		getline(cin, type);
		if (type.compare("-1") == 0) break;

		cout << endl;

		try
		{
			typesToHave.push_back(typeParser.parseEnum(capitalise(type)));
		}
		catch (runtime_error e)
		{
			cout << "Fucking give me a legit type faggot" << endl
				<< endl;
			continue;
		}
		counter++;
		cout << "Enter another type or -1 to stop" << endl;
	}

	vector<Pokemon*> pokemons;
	vector<Pokemon*> allPokemons = pokedex.getPokemons();
	for (vector<Pokemon*>::iterator pit = allPokemons.begin(); pit != allPokemons.end(); ++pit)
	{
		bool haveAll = true;
		for (vector<Type>::iterator tit = typesToHave.begin(); tit != typesToHave.end(); ++tit)
		{
			if (find((*pit)->types().begin(), (*pit)->types().end(), *tit) == (*pit)->types().end())
			{
				haveAll = false;
				break;
			}
		}
		if (haveAll)
			pokemons.push_back(*pit);
	}

	stringstream ss;

	if (typesToHave.empty())
	{
		cout << "You fucking faggot, you should have entered at least 1 Type" << endl;
		return;
	}
		
	ss << "Found no pokemons with types: ";
	for (vector<Type>::iterator tit = typesToHave.begin(); tit != typesToHave.end() - 1; ++tit)
		ss << *tit << ", ";
	ss << *(typesToHave.end() - 1);
	showPokemons(pokemons, ss.str());
}

/**
	Shows a dialog asking the user for details on the new 
	pokemon to be added.
*/
void showAddPokemonDialog()
{
	string input;
	cout << "Name of Pokemon: " << endl
		<< ">>> ";
	string name;
	getline(cin, name);
	
	cout << endl;

	double height;
	while (1)
	{
		cout << "Height of Pokemon: " << endl
			<< ">>> ";
		getline(cin, input);
		if (!tryParseInt(input))
		{
			cout << "Invalid height" << endl;
			continue;
		}

		height = stod(input);
		if (height <= 0)
		{
			cout << "Don't give me negative faggot" << endl;
			continue;
		}
		break;
	}

	cout << endl;

	double weight;
	while (1)
	{
		cout << "Weight of Pokemon: " << endl
			<< ">>> ";
		getline(cin, input);
		if (!tryParseInt(input))
		{
			cout << "Invalid weight" << endl;
			continue;
		}

		weight = stod(input);
		if (weight <= 0)
		{
			cout << "Don't give me negative faggot" << endl;
			continue;
		}
		break;
	}

	cout << endl;

	cout << "type -1 to finish adding abilities" << endl;
	cout << "Ensure each word is capitalised" << endl;
	int counter = 1;
	vector<Ability> abilities;
	EnumParser<Ability> abilityParser;
	while (1)
	{
		cout << "Ability " << counter << " of Pokemon: " << endl
			<< ">>> ";
		getline(cin, input);
		if (input.compare("-1") == 0) break;
		try
		{
			abilities.push_back(abilityParser.parseEnum(input));
			counter++;
		}
		catch (runtime_error e)
		{
			cout << "No such ability or you did not capitalise each word" << endl;
			continue;
		}
	}

	cout << endl;

	cout << "type -1 to finish adding types" << endl;
	cout << "Ensure each word is capitalised" << endl;
	counter = 1;
	vector<Type> types;
	EnumParser<Type> typeParser;
	while (1)
	{
		cout << "Type " << counter << " of Pokemon: " << endl
			<< ">>> ";
		getline(cin, input);
		if (input.compare("-1") == 0) break;
		try
		{
			types.push_back(typeParser.parseEnum(input));
			counter++;
		}
		catch (runtime_error e)
		{
			cout << "No such type or you did not capitalise each word" << endl;
			continue;
		}
	}

	pokedex.addPokemon(height, weight, name, abilities, types);
	cout << endl;
	cout << "Added " << name << endl;
}

/**
	Shows a dialog asking the user for details on the 
	pokemon to be edited.
*/
void showEditPokemonDialog()
{
	string input;
	int id;
	while (1)
	{
		cout << "Enter the id of the pokemon you want to edit" << endl
			<< ">>> ";
		getline(cin, input);
		if (!tryParseInt(input))
		{
			cout << "No pokemon with id " << input << endl;
			continue;
		}
		id = stoi(input);
		break;
	}

	cout << endl;

	Pokemon* pokemon = pokedex.getPokemon(id);
	
	cout << "New name of Pokemon: " << endl
		<< ">>> ";
	string name;
	getline(cin, name);

	cout << endl;

	double height;
	while (1)
	{
		cout << "New height of Pokemon: " << endl
			<< ">>> ";
		getline(cin, input);
		if (!tryParseInt(input))
		{
			cout << "Invalid height" << endl;
			continue;
		}

		height = stod(input);
		if (height <= 0)
		{
			cout << "Don't give me negative faggot" << endl;
			continue;
		}
		break;
	}

	cout << endl;

	double weight;
	while (1)
	{
		cout << "New weight of Pokemon: " << endl
			<< ">>> ";
		getline(cin, input);
		if (!tryParseInt(input))
		{
			cout << "Invalid weight" << endl;
			continue;
		}

		weight = stod(input);
		if (weight <= 0)
		{
			cout << "Don't give me negative faggot" << endl;
			continue;
		}
		break;
	}

	cout << endl;

	cout << "type -1 to finish adding new abilities" << endl;
	cout << "Ensure each word is capitalised" << endl;
	int counter = 1;
	vector<Ability> abilities;
	EnumParser<Ability> abilityParser;
	while (1)
	{
		cout << "Ability " << counter << " of Pokemon: " << endl
			<< ">>> ";
		getline(cin, input);
		if (input.compare("-1") == 0) break;
		try
		{
			abilities.push_back(abilityParser.parseEnum(input));
			counter++;
		}
		catch (runtime_error e)
		{
			cout << "No such ability or you did not capitalise each word" << endl;
			continue;
		}
	}

	cout << endl;

	cout << "type -1 to finish adding new types" << endl;
	cout << "Ensure each word is capitalised" << endl;
	counter = 1;
	vector<Type> types;
	EnumParser<Type> typeParser;
	while (1)
	{
		cout << "Type " << counter << " of Pokemon: " << endl
			<< ">>> ";
		getline(cin, input);
		if (input.compare("-1") == 0) break;
		try
		{
			types.push_back(typeParser.parseEnum(input));
			counter++;
		}
		catch (runtime_error e)
		{
			cout << "No such type or you did not capitalise each word" << endl;
			continue;
		}
	}

	pokemon->name(name);
	pokemon->height(height);
	pokemon->weight(weight);
	pokemon->abilities() = abilities;
	pokemon->types() = types;
	cout << endl;
	cout << "Edited pokemon " << id << endl;
}

/**
	Toggles the the output of showPokemons function between
	sorted by Id or Name.
*/
void toggleAlphabeticalSort()
{
	isSortAlphabetically = !isSortAlphabetically;
}

/**
	Sorts pokemons alphabetically.

	@param pokemons - The vector of pokemons to be sorted alphabectically
*/
void sortAlphabetically(vector<Pokemon*>& pokemons)
{
	sort(pokemons.begin(), pokemons.end(), [](const Pokemon* lhs, const Pokemon* rhs) {
		return lhs->name() < rhs->name();
	});
}

/**
	Capitalises each word in str. Assumes that all other characters
	are lower case already.

	@param str - String to be capitalised
	@return string - For lazy fucks
*/
string capitalise(string& str)
{
	if (str.empty()) return str;

	str[0] = toupper(str[0]);

	int startIndex = 0;
	int spaceIndex = str.find(' ', startIndex);
	// will break if there are leading and trailing spaces
	// lazy to account for those
	while (spaceIndex != string::npos)
	{
		startIndex = spaceIndex + 1;
		str[startIndex] = toupper(str[startIndex]);
		spaceIndex = str.find(' ', startIndex);
	}

	return str;
}

/**
	Removes all hypens in str.

	@param str - String to have its hypen removed
	@return string - For lazy fucks
*/
string removeHypen(string& str)
{
	for (string::iterator it = str.begin(); it != str.end(); ++it)
	{
		if ((*it) == '-')
		{
			(*it) = ' ';
		}
	}

	return str;
}

/**
	Tries to parse str into an int.

	@return bool - true = parsable, false = not parsable
*/
bool tryParseInt(string str)
{
	try {
		stoi(str);
	}
	catch (...)
	{
		return false;
	}
	return true;
}

/**
	Tries to parse str into an double.

	@return bool - true = parsable, false = not parsable
*/
bool tryParseDouble(string str)
{
	try {
		stod(str);
	}
	catch (...)
	{
		return false;
	}
	return true;
}