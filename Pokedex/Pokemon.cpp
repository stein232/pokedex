#include "Pokemon.h"

Pokemon::Pokemon(int number, double height, double weight, string name)
{
	number_ = number;
	height_ = height;
	weight_ = weight;
	name_ = name;
	//category_ = category;
}


int Pokemon::number() const
{
	return number_;
}

void Pokemon::number(int number)
{
	number_ = number;
}

double Pokemon::height() const
{
	return height_;
}

void Pokemon::height(double height)
{
	height_ = height;
}

double Pokemon::weight() const
{
	return weight_;
}

void Pokemon::weight(double weight)
{
	weight_ = weight;
}

string Pokemon::name() const
{
	return name_;
}

void Pokemon::name(string name)
{
	name_ = name;
}


//string Pokemon::category() const
//{
//	return category_;
//}
//
//void Pokemon::category(string category)
//{
//	category_ = category;
//}

vector<Ability>& Pokemon::abilities()
{
	return abilities_;
}

//vector<Gender>& Pokemon::genders()
//{
//	return genders_;
//}

vector<Type>& Pokemon::types()
{
	return types_;
}

//vector<Type>& Pokemon::weaknesses()
//{
//	return weaknesses_;
//}

//vector<Pokemon>& Pokemon::evolutions()
//{
//	return evolutions_;
//}

inline bool Pokemon::operator<(const Pokemon & rhs)
{
	return number_ < rhs.number_;
}

inline bool Pokemon::operator>(const Pokemon & rhs)
{
	return rhs.number_ < number_;
}

inline bool Pokemon::operator<=(const Pokemon & rhs)
{
	return !(rhs.number_ < number_);
}

inline bool Pokemon::operator>=(const Pokemon & rhs)
{
	return !(number_ < rhs.number_);
}

inline bool Pokemon::operator==(const Pokemon & rhs)
{
	return number_ == rhs.number_;
}

inline bool Pokemon::operator!=(const Pokemon & rhs)
{
	return !(number_ == rhs.number_);
}

ostream & operator<<(ostream& os, const Pokemon& pokemon)
{
	os << pokemon.name_ << endl;
	os << "Height: " << pokemon.height_ << " m" << endl;
	os << "Weight: " << pokemon.weight_ << " kg" << endl;
	//os << "Category: " << pokemon.category_ << endl;
	os << "Ability: ";
	for (vector<Ability>::const_iterator i = pokemon.abilities_.begin(); i != pokemon.abilities_.end() - 1; i++)
	{
		os << *i << ", ";
	}
	os << *(pokemon.abilities_.end() - 1) << endl;
	//os << "Gender: ";
	//for (vector<Gender>::const_iterator i = pokemon.genders_.begin(); i != pokemon.genders_.end() - 1; i++)
	//{
	//	os << *i << ", ";
	//}
	//os << *(pokemon.genders_.end() - 1) << endl;
	os << "Type: ";
	for (vector<Type>::const_iterator i = pokemon.types_.begin(); i != pokemon.types_.end() - 1; i++)
	{
		os << *i << ", ";
	}
	os << *(pokemon.types_.end() - 1) << endl;
	//os << "Weaknesses: ";
	//for (vector<Type>::const_iterator i = pokemon.weaknesses_.begin(); i != pokemon.weaknesses_.end() - 1; i++)
	//{
	//	os << *i << ", ";
	//}
	//os << *(pokemon.weaknesses_.end() - 1) << endl;
	return os;
}