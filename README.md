# DSAL Assignment AY 2016/2017 Semester 2 Solutions Development

A basic implementation of the DSAL assignment (Probably get a B/B+ if you submit this, but don't actually submit this, I don't want any trouble)

## Basics covered
* Can search
    * By Name
    * By Height
    * By Weight
    * By Ability
    * By Type
* Can add pokemon
* Can edit pokemon
* Can exit
* Encapsulation
    * Abstraction with .h and .cpp
    * Use of reference parameteres
    * Use of default parameters
    * Appropriate use of *const* keyword
    * Comments for functions and class (not that gay Pre and Post conditions)
* Pointers
    * Use *new* keyword
    * use *delete* keyword to free up memory
    * Appropriate use of shallow and/or deep copy
* STL
    * Use at least 1 STL container (vector)
    * Use iterators
* Bonus
    * File I/O
* *'Advanced'* features (actually intermediate at best)
    * Use lambda lol
    * EnumParser (check it out, I think quite useful)
    * Read from JSON (though it was with help of library)

## Basics not covered
* Can delete pokemon (forgot)
* Inheritance (Fuck that, my app is so fucking simple and the assignment technically does not require any inheritance)
* Pointers
    * Create virtual function (under inheritance, why here?)
    * Use of polymorphism (no inheritance, can't do)
* Bonus
    * Use linked list
    * Use any other data structures from the textbook or STL